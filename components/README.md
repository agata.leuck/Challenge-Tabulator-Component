# ptf-aleuck-tab-component

A simple tabs component with no styling.

## Instalation

Install with npm:

```
npm i ptf-aleuck-tab-component
```

Install with Yarn:

```
yarn add ptf-aleuck-tab-component
```

## Usage

### Uncontrolled mode

It can be readily used in uncontrolled mode, controlling the current tab and change internally:

```ts
import React from 'react'
import { Tab } from 'ptf-aleuck-tab-component'

export const MyTabsComponent = () => (
  <Tab>
    <Tab.Pane title="Tab 1">
      <p>First Pane Body</p>
    </Tab.Pane>
    <Tab.Pane title="Tab 2">
      <p>Second Pane Body</p>
    </Tab.Pane>
  </Tab>
)
```

### Controlled mode

Moreover it can be used in a more controlled manner:

```ts
import React, { useState } from 'react'
import { Tab } from 'ptf-aleuck-tab-component'

export const MyTabsComponent = () => {
  const [currentTab, setCurrentTab] = useState(0)
  return (
    <Tab active={currentTab} onActiveChange={setCurrentTab}>
      <Tab.Pane title="Tab 1">
        <p>First Pane Body</p>
      </Tab.Pane>
      <Tab.Pane title="Tab 2">
        <p>Second Pane Body</p>
      </Tab.Pane>
    </Tab>
  )
}
```
