import React from 'react'
import { Tab, TabProps } from './Tab'

export default {
  title: 'Tab',
  component: Tab,
}

const Template = (args: TabProps) => (
  <Tab {...args}>
    <Tab.Pane title="Pane 1">
      <p>First Pane Body</p>
    </Tab.Pane>
    <Tab.Pane title="Pane 2">
      <p>Second Pane Body</p>
    </Tab.Pane>
  </Tab>
)

export const Controlled = Template.bind({})
Controlled.args = {
  active: 0,
}

export const Uncontrolled = Template.bind({})
Uncontrolled.args = {
  initialActive: 0,
}
