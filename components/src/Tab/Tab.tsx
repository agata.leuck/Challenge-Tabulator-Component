import React, { useState } from 'react'

type Without<T, U> = { [P in Exclude<keyof T, keyof U>]?: never }
type XOR<T, U> = T | U extends object
  ? (Without<T, U> & U) | (Without<U, T> & T)
  : T | U

export type TabPaneProps = React.PropsWithChildren<{
  title: string
}>

interface BaseTabProps {
  children: React.ReactElement<TabPaneProps>[]
}

type ControlledTabProps = BaseTabProps & {
  active: number
  onActiveChange?: (value: number) => void
}

type UncontrolledTabProps = BaseTabProps & {
  initialActive?: number
}

export type TabProps = XOR<ControlledTabProps, UncontrolledTabProps>

function Tab({
  children,
  initialActive,
  active,
  onActiveChange,
}: TabProps): JSX.Element {
  const [stateCurrent, setStateCurrent] = useState(initialActive || 0)

  const current = active ?? stateCurrent
  const handleTabClick = (index: number) => () => {
    if (typeof active === 'number') {
      onActiveChange?.(index)
    } else {
      setStateCurrent(index)
    }
  }

  return (
    <>
      <div>
        {children.map((item, index) => (
          <button key={`tab-${index}`} onClick={handleTabClick(index)}>
            {item.props.title}
          </button>
        ))}
      </div>
      <div>{children[current]}</div>
    </>
  )
}

function TabPane(props: TabPaneProps): JSX.Element {
  return (
    <section>
      <h2>{props.title}</h2>
      <div>{props.children}</div>
    </section>
  )
}

Tab.Pane = TabPane

export { Tab }
