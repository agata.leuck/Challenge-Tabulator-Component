import React from 'react'
import { Tab2, Tab2Props } from './Tab2'

export default {
  title: 'Tab2',
  component: Tab2,
}

const TwoPanesTemplate = (args: Tab2Props) => (
  <Tab2 {...args}>
    <Tab2.Pane title="Pane 1">
      <p>First Pane Body</p>
    </Tab2.Pane>
    <Tab2.Pane title="Pane 2">
      <p>Second Pane Body</p>
    </Tab2.Pane>
  </Tab2>
)

const ManyPanesTemplate = (args: Tab2Props) => (
  <Tab2 {...args}>
    <Tab2.Pane title="Pane 1">
      <p>First Pane Body</p>
    </Tab2.Pane>
    <Tab2.Pane title="Pane 2">
      <p>Second Pane Body</p>
    </Tab2.Pane>
    <Tab2.Pane title="Pane 3">
      <p>Third Pane Body</p>
    </Tab2.Pane>
    <Tab2.Pane title="Pane 4">
      <p>Fourth Pane Body</p>
    </Tab2.Pane>
    <Tab2.Pane title="Pane 5">
      <p>Fifth Pane Body</p>
    </Tab2.Pane>
    <Tab2.Pane title="Pane 6">
      <p>Sixth Pane Body</p>
    </Tab2.Pane>
  </Tab2>
)

export const Controlled = TwoPanesTemplate.bind({})
Controlled.args = {
  active: 0,
}

export const Uncontrolled = TwoPanesTemplate.bind({})
Uncontrolled.args = {
  initialActive: 0,
}

export const UncontrolledWithManyPanes = ManyPanesTemplate.bind({})
UncontrolledWithManyPanes.args = {
  initialActive: 0,
}
