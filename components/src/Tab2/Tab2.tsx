import React, { useRef, useState } from 'react'

type Without<T, U> = { [P in Exclude<keyof T, keyof U>]?: never }
type XOR<T, U> = T | U extends object
  ? (Without<T, U> & U) | (Without<U, T> & T)
  : T | U

export type Tab2PaneProps = React.PropsWithChildren<{
  title: string
}>

interface BaseTab2Props {
  children: React.ReactElement<Tab2PaneProps>[]
}

type ControlledTab2Props = BaseTab2Props & {
  active: number
  onActiveChange?: (value: number) => void
}

type UncontrolledTab2Props = BaseTab2Props & {
  initialActive?: number
}

export type Tab2Props = XOR<ControlledTab2Props, UncontrolledTab2Props>

function Tab2({
  children,
  initialActive,
  active,
  onActiveChange,
}: Tab2Props): JSX.Element {
  const [stateCurrent, setStateCurrent] = useState<number>(initialActive || 0)

  const current = active ?? stateCurrent
  const handleTabClick = (index: number) => () => {
    if (typeof active === 'number') {
      onActiveChange?.(index)
    } else {
      setStateCurrent(index)
    }
  }

  const renderedRef = useRef<boolean[]>([])
  renderedRef.current[current] = true

  return (
    <>
      <div>
        {children.map((item, index) => (
          <button key={`tab-${index}`} onClick={handleTabClick(index)}>
            {item.props.title}
          </button>
        ))}
      </div>
      {children.map(
        (item, index) =>
          renderedRef.current[index] && (
            <div
              key={`pane-${index}`}
              style={{ display: index === current ? 'block' : 'none' }}
            >
              {item}
            </div>
          )
      )}
    </>
  )
}

function TabPane(props: Tab2PaneProps): JSX.Element {
  return (
    <section>
      <h2>{props.title}</h2>
      <div>{props.children}</div>
    </section>
  )
}

Tab2.Pane = TabPane

export { Tab2 }
