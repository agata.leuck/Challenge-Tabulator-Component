import { cleanup, fireEvent, render } from '@testing-library/react'
import renderer from 'react-test-renderer'
import React from 'react'
import { Tab2 as Tab } from './Tab2'

beforeEach(cleanup)

it('renders correctly with minimal props', () => {
  const component = renderer.create(
    <Tab>
      <Tab.Pane title="Tab 1">Pane 1 body</Tab.Pane>
      <Tab.Pane title="Tab 2">Pane 2 body</Tab.Pane>
    </Tab>
  )
  const tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})

describe('when in controlled mode', () => {
  it('renders correctly', () => {
    const cb = jest.fn()
    const component = renderer.create(
      <Tab active={0} onActiveChange={cb}>
        <Tab.Pane title="Tab 1">Pane 1 body</Tab.Pane>
        <Tab.Pane title="Tab 2">Pane 2 body</Tab.Pane>
      </Tab>
    )
    const tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('only renders first pane when `active={0}`', () => {
    const { queryByText } = render(
      <Tab active={0}>
        <Tab.Pane title="Tab 1">First pane body</Tab.Pane>
        <Tab.Pane title="Tab 2">Second pane body</Tab.Pane>
        <Tab.Pane title="Tab 3">Third pane body</Tab.Pane>
      </Tab>
    )
    expect(queryByText('First pane body')).toBeInTheDocument()
    expect(queryByText('Second pane body')).not.toBeInTheDocument()
    expect(queryByText('Third pane body')).not.toBeInTheDocument()
  })

  it('only renders second pane when `active={1}`', () => {
    const { queryByText } = render(
      <Tab active={1}>
        <Tab.Pane title="Tab 1">First pane body</Tab.Pane>
        <Tab.Pane title="Tab 2">Second pane body</Tab.Pane>
        <Tab.Pane title="Tab 3">Third pane body</Tab.Pane>
      </Tab>
    )
    expect(queryByText('First pane body')).not.toBeInTheDocument()
    expect(queryByText('Second pane body')).toBeInTheDocument()
    expect(queryByText('Third pane body')).not.toBeInTheDocument()
  })

  it('only renders third pane when `active={2}`', () => {
    const { queryByText } = render(
      <Tab active={2}>
        <Tab.Pane title="Tab 1">First pane body</Tab.Pane>
        <Tab.Pane title="Tab 2">Second pane body</Tab.Pane>
        <Tab.Pane title="Tab 3">Third pane body</Tab.Pane>
      </Tab>
    )
    expect(queryByText('First pane body')).not.toBeInTheDocument()
    expect(queryByText('Second pane body')).not.toBeInTheDocument()
    expect(queryByText('Third pane body')).toBeInTheDocument()
  })

  it('fires cb with 0 when click Tab 1', async () => {
    const cb = jest.fn()
    const { queryByText } = render(
      <Tab active={1} onActiveChange={cb}>
        <Tab.Pane title="Tab 1">First pane body</Tab.Pane>
        <Tab.Pane title="Tab 2">Second pane body</Tab.Pane>
        <Tab.Pane title="Tab 3">Third pane body</Tab.Pane>
      </Tab>
    )
    const tab1 = queryByText('Tab 1')
    expect(tab1).toBeInTheDocument()
    fireEvent.click(tab1)
    expect(cb).toHaveBeenCalledWith(0)
    expect(cb).toHaveBeenCalledTimes(1)
  })

  it('fires cb with 1 when click Tab 2', async () => {
    const cb = jest.fn()
    const { queryByText } = render(
      <Tab active={0} onActiveChange={cb}>
        <Tab.Pane title="Tab 1">First pane body</Tab.Pane>
        <Tab.Pane title="Tab 2">Second pane body</Tab.Pane>
        <Tab.Pane title="Tab 3">Third pane body</Tab.Pane>
      </Tab>
    )
    const tab2 = queryByText('Tab 2')
    expect(tab2).toBeInTheDocument()
    fireEvent.click(tab2)
    expect(cb).toHaveBeenCalledWith(1)
    expect(cb).toHaveBeenCalledTimes(1)
  })

  it('fires cb with 2 when click Tab 3', async () => {
    const cb = jest.fn()
    const { queryByText } = render(
      <Tab active={0} onActiveChange={cb}>
        <Tab.Pane title="Tab 1">First pane body</Tab.Pane>
        <Tab.Pane title="Tab 2">Second pane body</Tab.Pane>
        <Tab.Pane title="Tab 3">Third pane body</Tab.Pane>
      </Tab>
    )
    const tab3 = queryByText('Tab 3')
    expect(tab3).toBeInTheDocument()
    fireEvent.click(tab3)
    expect(cb).toHaveBeenCalledWith(2)
    expect(cb).toHaveBeenCalledTimes(1)
  })
})

describe('when in uncontrolled mode', () => {
  it('renders correctly', () => {
    const component = renderer.create(
      <Tab initialActive={1}>
        <Tab.Pane title="Tab 1">Pane 1 body</Tab.Pane>
        <Tab.Pane title="Tab 2">Pane 2 body</Tab.Pane>
      </Tab>
    )
    const tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('only renders first pane when `initialActive={0}`', () => {
    const { queryByText } = render(
      <Tab initialActive={0}>
        <Tab.Pane title="Tab 1">First pane body</Tab.Pane>
        <Tab.Pane title="Tab 2">Second pane body</Tab.Pane>
        <Tab.Pane title="Tab 3">Third pane body</Tab.Pane>
      </Tab>
    )
    expect(queryByText('First pane body')).toBeInTheDocument()
    expect(queryByText('Second pane body')).not.toBeInTheDocument()
    expect(queryByText('Third pane body')).not.toBeInTheDocument()
  })

  it('only renders second pane when `initialActive={1}`', () => {
    const { queryByText } = render(
      <Tab initialActive={1}>
        <Tab.Pane title="Tab 1">First pane body</Tab.Pane>
        <Tab.Pane title="Tab 2">Second pane body</Tab.Pane>
        <Tab.Pane title="Tab 3">Third pane body</Tab.Pane>
      </Tab>
    )
    expect(queryByText('First pane body')).not.toBeInTheDocument()
    expect(queryByText('Second pane body')).toBeInTheDocument()
    expect(queryByText('Third pane body')).not.toBeInTheDocument()
  })

  it('only renders third pane when `initialActive={2}`', () => {
    const { queryByText } = render(
      <Tab initialActive={2}>
        <Tab.Pane title="Tab 1">First pane body</Tab.Pane>
        <Tab.Pane title="Tab 2">Second pane body</Tab.Pane>
        <Tab.Pane title="Tab 3">Third pane body</Tab.Pane>
      </Tab>
    )
    expect(queryByText('First pane body')).not.toBeInTheDocument()
    expect(queryByText('Second pane body')).not.toBeInTheDocument()
    expect(queryByText('Third pane body')).toBeInTheDocument()
  })

  it('adds panes to the dom as they are accessed', async () => {
    const { queryByText } = render(
      <Tab initialActive={0}>
        <Tab.Pane title="Tab 1">First pane body</Tab.Pane>
        <Tab.Pane title="Tab 2">Second pane body</Tab.Pane>
        <Tab.Pane title="Tab 3">Third pane body</Tab.Pane>
        <Tab.Pane title="Tab 4">Fourth pane body</Tab.Pane>
      </Tab>
    )
    expect(queryByText('First pane body')).toBeInTheDocument()
    expect(queryByText('Second pane body')).not.toBeInTheDocument()
    expect(queryByText('Third pane body')).not.toBeInTheDocument()
    expect(queryByText('Fourth pane body')).not.toBeInTheDocument()

    const tab3 = queryByText('Tab 3')
    expect(tab3).toBeInTheDocument()
    fireEvent.click(tab3)

    expect(queryByText('First pane body')).toBeInTheDocument()
    expect(queryByText('Second pane body')).not.toBeInTheDocument()
    expect(queryByText('Third pane body')).toBeInTheDocument()
    expect(queryByText('Fourth pane body')).not.toBeInTheDocument()

    const tab4 = queryByText('Tab 4')
    expect(tab4).toBeInTheDocument()
    fireEvent.click(tab4)

    expect(queryByText('First pane body')).toBeInTheDocument()
    expect(queryByText('Second pane body')).not.toBeInTheDocument()
    expect(queryByText('Third pane body')).toBeInTheDocument()
    expect(queryByText('Fourth pane body')).toBeInTheDocument()
  })
})
