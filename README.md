# ptf-aleuck-tab-component

<p style="align:center;vertical-align:center;">
![npm](https://badgen.net/npm/v/ptf-aleuck-tab-component)
![Tree-shaking](https://badgen.net/bundlephobia/tree-shaking/ptf-aleuck-tab-component)
![Types](https://badgen.net/npm/types/ptf-aleuck-tab-component)
</p>

For this project I didn't use a starter since I didn't really wish to deploy an app. Deploying a component library with a storybook to run it on made more sense. Thus so, my approach was a monorepo architecture to have a nice separation between the publishable component library, the storybook and a possible App to deploy, while keeping a smooth development environment with each package being able to access its local counterpart.

## Running locally

1. Run `yarn install` to install project;
2. Run `yarn sb` to run storybook; and
3. Access http://localhost:6006/.

## Using in another project

Instructions [here](https://www.npmjs.com/package/ptf-aleuck-tab-component).

## Bonus Task 1 (react+)

Created `Tab2` component.

## Bonus Task 2 (ts+)

> Update types to prevent out-of-bound active / initialActive values (if it's technically possible). Like the following should be a type error:

It is not possible to check out-of-bounds values during typecheck in Typescript alone because:

- Typescript only does static type checking (does not typecheck during runtime). Thus, it would require the number of panes to be defined at compile time, making impossible to load an arbritrary number of panes dynamically (eg.: from an endpoint);
- Typescript does not support dependant types (types that depends on terms);

However, it is possible to implement runtime type checking. And there are some neat libraries out there that help with that, like [io-ts](https://www.npmjs.com/package/io-ts) and [Zod](https://www.npmjs.com/package/zod).

Using of these libraries above would be overkill for this solution. Moreover, I don't think runtime errors are desirable. In case of a out-of-bounds situation, it is better to keep the current behaviour (don't show any pane) and log the issue into a logging service.

## Bonus Task 3 (qa+)

Unit tests running on Jest.

## Bonus Task 4 (fe+)

Published as NPM package [`ptf-aleuck-tab-component`](https://www.npmjs.com/package/ptf-aleuck-tab-component).

Storybook published at Vercel: https://challenge-tabulator-component.vercel.app/
